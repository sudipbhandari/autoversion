package com.example.sudip.autoversion;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.YuvImage;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import java.io.File;





public class MainActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 4; //request code
    public static String EXTRA_MESSAGE=new String();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
   }

    public void sendMessage(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        String EXTRA_MESSAGE=new String();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;
    public void takePicture (View view) {
        //pass an intent to take images to another activity
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Result","after capturing image");
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.d("Result","image returned from camera");
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView mImageView = new ImageView(this);
            mImageView.setImageBitmap(imageBitmap);

        }

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            //get images and encode them
            Log.d("images recieved","images received from intent");
            /*Bundle extras = data.getExtras();
            Log.i("Extras",extras.getClass().getName());*/
        }

    }

    public void openBrowser(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        startActivity(intent);
    }

    public void callUs(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "9894358069"));
        startActivity(intent);
    }


    public void encodeImages(View view) throws IOException
        {
            MediaCodec codec = MediaCodec.createEncoderByType("video/avc");

            //
            MediaFormat mediaFormat = MediaFormat.createVideoFormat("video/avc", 1280, 720);
            mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, 700000);
            mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 30);

            //not all phones support given color format, if color format is not supported app will crash with mediaCodec exception
            mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar);
            mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);

            codec.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);


            codec.start();

            ByteBuffer[] inputBuffers = codec.getInputBuffers();
            ByteBuffer[] outputBuffers = codec.getOutputBuffers();


            File ff = new File(Environment.getExternalStorageDirectory(), "Download/video_encoded.264");
            if (!ff.exists()) ff.createNewFile();
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(ff));

            File fff = new File(Environment.getExternalStorageDirectory().toString()+ "/images/out.yuv");
            FileInputStream fileInputStream = new FileInputStream(fff);
            int byteLength = (int) 1382400;
            byte[] filecontent = new byte[byteLength];
            fileInputStream.read(filecontent,0,byteLength); //check the limit buffer

            for (; ; )
            {
                int inputBufferId = codec.dequeueInputBuffer(-1);
                if (inputBufferId >= 0) {
                    // fill inputBuffers[inputBufferId] with valid data
                    try {
                        inputBuffers[inputBufferId].clear();
                        inputBuffers[inputBufferId].put(filecontent);
                    }
                    catch (Exception e)
                    {
                        System.out.print(e);
                    }
                    codec.queueInputBuffer(inputBufferId, 0, byteLength, System.nanoTime(),  0);
                }

                MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();


                int outputBufferId = codec.dequeueOutputBuffer(bufferInfo, -1);

                if (outputBufferId >= 0) {
                    // outputBuffers[outputBufferId] is ready to be processed or rendered.


                    ByteBuffer outputBuffer = outputBuffers[outputBufferId];
                    byte[] outData = new byte[bufferInfo.size];
                    outputBuffer.get(outData);


                    outputStream.write(outData, 0, outData.length);
                    outputStream.flush();


                    codec.releaseOutputBuffer(outputBufferId, false);
                    if (bufferInfo.flags == MediaCodec.BUFFER_FLAG_END_OF_STREAM ) break;


                } else if (outputBufferId == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    outputBuffers = codec.getOutputBuffers();
                } else if (outputBufferId == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    // Subsequent data will conform to new format.
                    MediaFormat format = codec.getOutputFormat();
                }
            }
            codec.stop();
            codec.release();

            outputStream.flush();
            outputStream.close();
        }





















 public void convert(View view){
     Bitmap mBitmap = BitmapFactory.decodeFile("");
// mBitmap is your bitmap

     int mWidth = mBitmap.getWidth();
     int mHeight = mBitmap.getHeight();

     //one way to do it
     byte[] yuv = getNV21(mWidth, mHeight, mBitmap);


     //another way to do it
     int bytes = mBitmap.getByteCount();
     ByteBuffer buffer = ByteBuffer.allocate(bytes);
     mBitmap.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

     byte[] data = buffer.array(); //Get the bytes array of the bitmap
     YuvImage yuvImage = new YuvImage(data, ImageFormat.NV21, mWidth, mHeight, null);

    }

    // untested function
    byte [] getNV21(int inputWidth, int inputHeight, Bitmap scaled) {

        int [] argb = new int[inputWidth * inputHeight];

        scaled.getPixels(argb, 0, inputWidth, 0, 0, inputWidth, inputHeight);

        byte [] yuv = new byte[inputWidth*inputHeight*3/2];
        encodeYUV420SP(yuv, argb, inputWidth, inputHeight);
        scaled.recycle();

        return yuv;
    }

    void encodeYUV420SP(byte[] yuv420sp, int[] argb, int width, int height) {
        final int frameSize = width * height;

        int yIndex = 0;
        int uvIndex = frameSize;

        int a, R, G, B, Y, U, V;
        int index = 0;
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {

                a = (argb[index] & 0xff000000) >> 24; // a is not used obviously
                R = (argb[index] & 0xff0000) >> 16;
                G = (argb[index] & 0xff00) >> 8;
                B = (argb[index] & 0xff) >> 0;

                // well known RGB to YUV algorithm
                Y = ( (  66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
                U = ( ( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
                V = ( ( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;

                // NV21 has a plane of Y and interleaved planes of VU each sampled by a factor of 2
                //    meaning for every 4 Y pixels there are 1 V and 1 U.  Note the sampling is every other
                //    pixel AND every other scanline.
                yuv420sp[yIndex++] = (byte) ((Y < 0) ? 0 : ((Y > 255) ? 255 : Y));
                if (j % 2 == 0 && index % 2 == 0) {
                    yuv420sp[uvIndex++] = (byte)((V<0) ? 0 : ((V > 255) ? 255 : V));
                    yuv420sp[uvIndex++] = (byte)((U<0) ? 0 : ((U > 255) ? 255 : U));
                }

                index ++;
            }
        }
    }


    public void encodeImagesasdf(View view) throws IOException {

        MediaCodec mediaCodec = null;
        byte[] input = new byte[1000];
        BufferedOutputStream outputStream = null;
        FileInputStream fileInputStream=null;

        try {
            //TODO
            //adjust parameters by consulting with hari sir

            mediaCodec = MediaCodec.createEncoderByType("video/avc");

            //
            MediaFormat mediaFormat = MediaFormat.createVideoFormat("video/avc", 1280, 720);
            mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, 700000);
            mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 30);

            //not all phones support given color format, if color format is not supported app will crash with mediaCodec exception
            mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar);
            mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 5);

            mediaCodec.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);


            mediaCodec.start();
            //after the mediaCodec is started we don't have ownership of input or output buffers

            System.out.println("Codecinfo" + mediaCodec.getCodecInfo());
            System.out.println("Codecname" + mediaCodec.getName());

        }
        catch (Exception e)
        {
            Log.e("ExceptionMediaCodec","Some exception in media codec");
        }

        //reached here
        System.out.println("mediacodec info=" + mediaCodec.getCodecInfo());


        try {
            File ff = new File(Environment.getExternalStorageDirectory(), "Download/video_encoded.264");
            if (!ff.exists()) ff.createNewFile();
            System.out.println("H.264 output file initialized");

            outputStream = new BufferedOutputStream(new FileOutputStream(ff));
            System.out.println("H264 avc Encoder --> outputStream initialized");
        } catch (Exception e){
            e.printStackTrace();
        }


        //trying different size of images from different location
        String path = Environment.getExternalStorageDirectory().toString()+"/images";
        File f = new File(path);
        Log.i("ExternalFileInfo",path.toString());

        //read image files onto an array
        File[] files = f.listFiles();
        System.out.println(files.getClass().getName());

        int NUM_IMAGES = files.length;
        System.out.println("files ="+files.length);

        String[] images = new String[NUM_IMAGES];

//        for (int i=0;i<NUM_IMAGES;i++)
//                images[i]=files[i].getName();
//
//        for (String eachimage: images) {
//            System.out.println("eachimage=>" + eachimage);
//            byte[] eachByte = eachimage.getBytes();
//            input = eachByte; //demo
//            System.out.println("input byte initialized"+input.toString());
//            try {
//                System.out.println("Following is the content of byte array input");
//                System.out.write(input);
//            }catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//
//
//        }
//
//        //reached here
//        System.out.println("image byte size="+input.length);
//        //all images converted to bytearray

        //what we get is a buffer array
        ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
        ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();

        System.out.println("number of imput buffers=" + inputBuffers.length);
        for (int k = 0; k<inputBuffers.length;k++)
        {
            System.out.println("input buffer"+(k+1)+" capacity="+inputBuffers[k].capacity());
        }

        //trying to get input buffer beforehand

        //System.out.println("inputBuffers="+(inputBuffers));
        //System.out.println("outputBuffers="+(outputBuffers));
        //reached here

        //returns the index of input buffer to be filled for encoding
        int inputBufferIndex = mediaCodec.dequeueInputBuffer(-1); //-1 => wait indefinitely

        System.out.println("inputBufferedIndex=" + inputBufferIndex); //0


        int fileindex=0;
       while (inputBufferIndex >= 0) {

            if (fileindex== files.length)
            {
                System.out.println("breaking from input buffer");
                break;
            }

            File image = files[fileindex++];
            try
            {
                 fileInputStream = new FileInputStream(image);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            System.out.println("Image inside == " + image.getAbsolutePath());
            //image is the image file
            //convert it into byte array and feed the encoder

            int byteLength = (int) image.length();
            byte[] filecontent = new byte[byteLength];
            fileInputStream.read(filecontent); //check the limit buffer

            System.out.println("Image length" + image.length());


            ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
            inputBuffer.clear();
            inputBuffer.put(filecontent);

            //System.out.println("inputBuffer after filling up" + inputBuffer);
           if (fileindex < files.length)
            mediaCodec.queueInputBuffer(inputBufferIndex, 0, input.length, System.nanoTime(),  0); //send each request with different timestamp
           else
           mediaCodec.queueInputBuffer(inputBufferIndex, 0, input.length, System.nanoTime(),  MediaCodec.BUFFER_FLAG_END_OF_STREAM); //send each request with different timestamp


           System.out.println("mediacodec input  queued");

            inputBufferIndex = mediaCodec.dequeueInputBuffer(-1);
            System.out.println("inputBufferIndex value="+inputBufferIndex);
        }


        System.out.println("i============="+fileindex);

        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        System.out.println("buffer info="+bufferInfo);

        int outputBufferIndex = -1;
        //reached here
        System.out.println("buffer info meta data=" + bufferInfo);
        System.out.println("outputBufferedIndex=" + outputBufferIndex);
        System.out.println("-2 stands for" + mediaCodec.INFO_OUTPUT_FORMAT_CHANGED);

        //info output format changed.. (next action)?
            int outputbuffercount = 0;
            try {
                while (true)
                {
                    System.out.println("output buffer index bef " + outputBufferIndex);
                    System.out.flush();
                    outputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, -1);
                    System.out.println("output buffer index " + outputBufferIndex);
                    System.out.flush();

                    if (outputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED ) {
                        outputBuffers = mediaCodec.getOutputBuffers();
                        continue;
                    }

                    if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        // Subsequent data will conform to new format.
                        // Can ignore if using getOutputFormat(outputBufferId)
                        //outputFormat = mediaCodec.getOutputFormat(); // option B
                        continue;
                    }

                    if (outputBufferIndex < 0) continue;


                    System.out.println("output buffer index is greater than or equal to zero=="+outputBufferIndex);
                    outputbuffercount++;

                    ByteBuffer outputBuffer = outputBuffers[outputBufferIndex];
                    byte[] outData = new byte[bufferInfo.size];
                    outputBuffer.get(outData);

                    System.out.println("buffer length = "+outData);

                    outputStream.write(outData, 0, outData.length);
                    outputStream.flush();
                    System.out.println("AvcEncoder"+ outData.length + " bytes written");

                    mediaCodec.releaseOutputBuffer(outputBufferIndex, false);

                    //trying to flush beforehand
                    outputStream.flush();

                    if (bufferInfo.flags == MediaCodec.BUFFER_FLAG_END_OF_STREAM ) break;

                }
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }
        System.out.println("Output buffercount="+outputbuffercount);


        try
        {
            mediaCodec.stop();
            mediaCodec.release();

            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("Mediacodec="+mediaCodec);


        if (files==null) System.out.println("filenames null");
        System.out.println("Number of images="+files.length);
    }


    public void startEncoding(View view){

        //image picker intent
        try {
            Intent chooseIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            chooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            startActivityForResult(chooseIntent, PICK_IMAGE);
        //TODO
        //get images from sdcard into bytebuffer
        //encode them using mediacode
        //convert raw h.262 into mp4
        //save and upload to peacock

            //MediaCodec mMediaCodec = MediaCodec.createEncoderByType("video/avc");
        }
        catch (Exception e)
        {
            Log.e("Error in MediaCodec","Some exception occured in MediaCodec");
        }
    }

}



